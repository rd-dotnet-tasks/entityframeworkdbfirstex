﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFrameworkDBFirstEx
{
    class Program
    {
        static void Main(string[] args)
        {
            using(var ctx = new AdventureWorks2017Entities())
            {
                var peopleList = (from s in ctx.People
                                orderby s.FirstName
                                select s).Take(10);

                foreach (var people in peopleList)
                {
                    Console.WriteLine("First Name: {0}, Last Name: {1}", people.FirstName, people.LastName);
                }

                Console.ReadKey();

            }
        }
    }
}
